// Fibaro Melview Platform plugin for HomeBridge
//
// Remember to add platform to config.json. Example:
// "platforms": [
// 	{
// 		"platform": "Melview",
// 		"name": "Melview",
// 		"username": "username",
// 		"password": "password",
// 		"cookie": "auth=XXXX",
// 		"cookie-expiry": "Thu, 02-Aug-2018 12:57:59 GMT",
// 	}
// ]
//
// When you attempt to add a device, it will ask for a "PIN code".
// The default code for all HomeBridge accessories is 031-45-154.

'use strict';

var Service, Characteristic;
var request = require("request");

var verticalTiltAngle = {
	1: 0,
	2: -22.5,
	3: -45,
	4: -67.5,
	5: -90
};

var horizontalTiltAngle = {
	1: 45,
	2: 22.5,
	3: 0,
	4: -22.5,
	5: -45
};

function MelviewPlatform(log, config){
	this.log      = log;
	this.username = config["username"];
	this.password = config["password"];
	this.cookie = config["cookie"];
  this.cookieExpiry = config["cookie-expiry"];
	this.fetchingInterval = config["fetchingInterval"];
	// this.UseFahrenheit = null;
	this.CurrentHeatingCoolingStateUUID = (new Characteristic.CurrentHeatingCoolingState()).UUID;
	this.TargetHeatingCoolingStateUUID = (new Characteristic.TargetHeatingCoolingState()).UUID;
	this.CurrentTemperatureUUID = (new Characteristic.CurrentTemperature()).UUID;
	this.TargetTemperatureUUID = (new Characteristic.TargetTemperature()).UUID;
	this.TemperatureDisplayUnitsUUID = (new Characteristic.TemperatureDisplayUnits()).UUID;
	this.RotationSpeedUUID = (new Characteristic.RotationSpeed()).UUID;
	this.CurrentHorizontalTiltAngleUUID = (new Characteristic.CurrentHorizontalTiltAngle()).UUID;
	this.TargetHorizontalTiltAngleUUID = (new Characteristic.TargetHorizontalTiltAngle()).UUID;
	this.CurrentVerticalTiltAngleUUID = (new Characteristic.CurrentVerticalTiltAngle()).UUID;
	this.TargetVerticalTiltAngleUUID = (new Characteristic.TargetVerticalTiltAngle()).UUID;
}

module.exports = function(homebridge) {
	if (homebridge) {
		Service = homebridge.hap.Service;
	  Characteristic = homebridge.hap.Characteristic;

	  homebridge.registerPlatform("homebridge-melview", "Melview", MelviewPlatform);
	}
}

MelviewPlatform.prototype = {
	logIn: function(callback){
		const self = this;

		self.log("Logging in Melview...");

		const url = "https://api.melview.net/api/login.aspx";

		request({
			url: url,
			headers: {"Content-Type": "application/json"},
			method: "post",
			timeout: 15000,
			form: JSON.stringify({
				user: self.username,
				pass: self.password,
			})
		}, function(err, response) {
			var result;
			try {
				if (err) {
					throw(err);
				}

				var rawcookies = response.headers['set-cookie'];

				//this.cookie = rawcookies.shift().split(";").shift();
				//console.log("=====cookie======", this.cookie);
				var cookies = rawcookies.shift().split(";")


				self.cookie = cookies[0];
				self.cookieExpiry = cookies[2].replace("expires=", "");


				console.log("cookie:", self.cookie);
				console.log("!cookieExpiry:", self.cookieExpiry);

			} catch(e) {
				self.log(e);
			} finally {
				self.log(response.headers);
				self.log(response.body);

				if (callback) {
						callback();
				}
			}
		});
	},
	callAPI: function(path, params, callback) {
		const self = this;

		if (self.cookie
      && self.cookieExpiry
      && new Date(self.cookieExpiry).getTime() >= new Date().getTime())
			{
				const url = `https://api.melview.net/api/${path}.aspx`;

				self.log("calling " + url + " " + JSON.stringify(params));

				request({
					url: url,
					headers: {"Content-Type": "application/json", "Cookie": self.cookie},
					method: "post",
					timeout: 15000,
					form: JSON.stringify(params)
				}, function(err, response) {
					var result;
					try {
						if (err) {
							throw(err);
						}

						result = JSON.parse(response.body);
						self.log(result);
					} catch(e) {
						self.log(e);
					} finally {
						callback(null, result);
					}
				});
    } else {
			self.logIn(function() {
				self.callAPI(path, params, callback);
			});
		}
	},
  accessories: function(callback) {
		const self = this;
		self.createAccessories(callback);
  },
  createAccessories: function(callback) {
		const self = this;

		self.callAPI("rooms", {}, function(err, buildings) {
			/* rooms return example:
			[{
				"buildingid":"12864",
				"building":"Building",
				"bschedule":"0",
				"units":[{
					"room":"Lounge",
					"unitid":"119563",
					"power":"q",
					"wifi":"1",
					"mode":"3",
					"temp":"20",
					"settemp":"16",
					"status":"",
					"schedule1":0
				}]
			}]
			*/
			if (err) {
				self.log(err);

				setTimeout(function(){
					self.createAccessories(callback);
				}, 10000);
			}

			self.units = [];
			self.foundAccessories = [];

			if (buildings) {
				buildings.forEach(function(building){
					building.units.forEach(function(unit){
						self.units.push(unit);
					});
				});
			}

			self.units.forEach(function(unit){
				self.callAPI("unitcapabilities", {unitid:unit.unitid}, function(err, capabilities){
					/* unitcapabilities return example:
					{
						"id":"119563",
						"unitname":"Lounge",
						"unittype":"RAC",
						"userunits":1,
						"modeltype":1,
						"adaptortype":"mac559",
						"localip":"192.168.2.17",
						"fanstage":5,
						"hasairdir":1,
						"hasswing":1,
						"hasautomode":1,
						"hasautofan":1,
						"hasdrymode":1,
						"hasoutdoortemp":0,
						"hasairauto":1,
						"hasairdirh":0,
						"max":{
							"3":{"min":16,"max":31},
							"1":{"min":10,"max":31},
							"8":{"min":16,"max":31}
						},
						"time":"19:18 Sat",
						"error":
						"ok"
					}
					*/
					unit.capabilities = capabilities;

					var controlService = new Service.Thermostat(capabilities.unitname);
					var characteristics = [
						Characteristic.CurrentHeatingCoolingState,
						Characteristic.TargetHeatingCoolingState,
						Characteristic.CurrentTemperature,
						Characteristic.TargetTemperature,
						Characteristic.TemperatureDisplayUnits,
						Characteristic.RotationSpeed,
						Characteristic.CurrentVerticalTiltAngle,
						Characteristic.CurrentHorizontalTiltAngle,
						Characteristic.SwingMode
					];

					if (capabilities.hasairdir) {
						characteristics.push(Characteristic.TargetVerticalTiltAngle);
					}

					if (capabilities.hasairdirh) {
						characteristics.push(Characteristic.TargetHorizontalTiltAngle);
					}

					var accessory = new MelviewBridgedAccessory([
						{
							controlService,
							characteristics
						}
					]);

					accessory.platform 				= self;
					accessory.id 				  		= unit.unitid;
					accessory.name						= unit.room + " " + capabilities.unittype;
					accessory.model						= capabilities.unittype;
					accessory.manufacturer		= "Mitsubishi";
					accessory.serialNumber		= unit.unitid;

					accessory.capabilities = capabilities;
					accessory.state = {};


					self.log("Found unit: " + unit.room);

					unit.accessory = accessory;
					self.foundAccessories.push(accessory);

					self.getState(unit);

					if (self.fetchingInterval && self.fetchingInterval > 0) {
						setInterval(function(){

							if (unit.isFetching ) {
								return;
							}

							if (unit.lastUpdate != null && unit.lastUpdate - new Date().getTime() > -self.fetchingInterval ) {
								return;
							}

							self.getState(unit);
						}, 60000);
					}

					if (self.foundAccessories.length === self.units.length) {
						callback(self.foundAccessories);
					}
				});
			});
		});
  },
	getState: function(unit, callback){
		const self = this;
		unit.isFetching = true;

		self.callAPI("unitcommand", {unitid:unit.unitid,v:2}, function(err, state){
			/*
			{
				"id":"119563",
				"power":0,
				"standby":0,
				"setmode":3,
				"automode":0,
				"setfan":0,
				"settemp":"16",
				"roomtemp":"22",
				"airdir":0,
				"airdirh":12,
				"sendcount":0,
				"fault":"",
				"error":"ok"
			}
			*/
			if (!err) {
				unit.accessory.state = state
				unit.lastUpdate = new Date();
			}

			unit.isFetching = false;

			if (callback) {
				callback();
			}
		});
	},
	sendCommand: function(unit, commands, callback){
		const self = this;

		self.callAPI("unitcommand", {unitid:unit.unitid,v:2,lc:1,commands:commands}, function(err, state){
			/*
			{
				"id":"119563",
				"lc":"fc41013010010100000000000000000000000000007c",
				"power":0,
				"standby":0,
				"setmode":3,
				"automode":0,
				"setfan":0,
				"settemp":"16",
				"roomtemp":"22",
				"airdir":0,
				"airdirh":12,
				"sendcount":0,
				"fault":"",
				"error":"ok"
			}
			*/
			if (err) {
				self.log(err);
			} else {
				unit.accessory.state = state
				unit.lastUpdate = new Date();

				const ip = unit.accessory.capabilities.localip;
				const lc = state.lc;

				if (ip && lc) {
					self.sendLocalCommand(ip, lc, callback);
					return;
				}
			}

			callback();
		});
	},
	sendLocalCommand: function(ip, lc, callback){
		const self = this;

		const url = `http://${ip}/smart`;

		self.log("sending local command to " + url);

		request({
			url: url,
			headers: {"Content-Type": "text/plain;charset=UTF-8"},
			method: "post",
			timeout: 15000,
			body: `<?xml version="1.0" encoding="UTF-8"?><CSV><CONNECT>ON</CONNECT><CODE><VALUE>${lc}</VALUE></CODE></CSV>`
		}, function(err, response) {
			var result;
			try {
				if (err) {
					throw(err);
				}

				result = response.body;
				self.log(result);
			} catch(e) {
				self.log(e);
			} finally {
				callback();
			}
		});
	},
  getAccessoryValue: function(callback, characteristic, service, homebridgeAccessory) {
		const self = this;

		const unit = this.units.find(function(unit){
			return homebridgeAccessory.id == unit.unitid;
		})

		if (!unit){
			callback()
			return;
		}

		if (unit.isFetching) {
			setTimeout(function(){
				self.getAccessoryValue(callback, characteristic, service, homebridgeAccessory);
			}, 1000);
			return;
		} else if (unit.lastUpdate == null || unit.lastUpdate - new Date().getTime() < -10000 ) {
			self.getState(unit, function() {
				self.getAccessoryValue(callback, characteristic, service, homebridgeAccessory);
			}) ;
			return;
		}

  	const r = homebridgeAccessory.state;

  	if (characteristic.UUID == homebridgeAccessory.platform.CurrentHeatingCoolingStateUUID) {
  		if (!r.power) {
  			callback(undefined, Characteristic.CurrentHeatingCoolingState.OFF);
  			return;
  		} else {
  			switch (r.setmode) {
  				case 1:
  					callback(undefined, Characteristic.CurrentHeatingCoolingState.HEAT);
  					return;
  				case 3:
  					callback(undefined, Characteristic.CurrentHeatingCoolingState.COOL);
  					return;
          case 8:
            callback(undefined, Characteristic.CurrentHeatingCoolingState.AUTO);
            return;
  				default:
  					// Melview can return also 2 (deumidity), 7 (Ventilation), 8 (auto)
  					// We try to return 5 which is undefined in homekit
  					callback(undefined, 5);
  					return;
  			}
  		}
  	} else if (characteristic.UUID == homebridgeAccessory.platform.TargetHeatingCoolingStateUUID) {
  		if (!r.power) {
  			callback(undefined, Characteristic.TargetHeatingCoolingState.OFF);
  			return;
  		} else {
  			switch (r.setmode) {
  				case 1:
  					callback(undefined, Characteristic.TargetHeatingCoolingState.HEAT);
  					return;
  				case 3:
  					callback(undefined, Characteristic.TargetHeatingCoolingState.COOL);
  					return;
  				case 8:
  					callback(undefined, Characteristic.TargetHeatingCoolingState.AUTO);
  					return;
  				default:
  					// Melview can return also 2 (deumidity), 7 (Ventilation)
  					// We try to return 5 which is undefined in homekit
  					callback(undefined, 5);
  					return;
  			}
  		}
  	} else if (characteristic.UUID == homebridgeAccessory.platform.CurrentTemperatureUUID) {
  		callback(undefined, parseFloat(r.roomtemp));
  		return;
  	} else if (characteristic.UUID == homebridgeAccessory.platform.TargetTemperatureUUID) {
  		callback(undefined, parseFloat(r.settemp));
  		return;
  	} else if (characteristic.UUID == homebridgeAccessory.platform.TemperatureDisplayUnitsUUID) {
  		if (homebridgeAccessory.platform.UseFahrenheit) {
  			callback(undefined, Characteristic.TemperatureDisplayUnits.FAHRENHEIT);
  		} else {
  			callback(undefined, Characteristic.TemperatureDisplayUnits.CELSIUS);
  		}
  		return;
  	} else if (characteristic.UUID == homebridgeAccessory.platform.RotationSpeedUUID) {
  		var fan = r.setfan;
			var stage = homebridgeAccessory.capabilities.fanstage;
			if (fan && stage) {
				callback(undefined, fan / stage * 100.0);
			} else {
				callback(undefined, undefined);
			}
  		return;
  	} else if (characteristic.UUID == homebridgeAccessory.platform.CurrentHorizontalTiltAngleUUID ||
  			   characteristic.UUID == homebridgeAccessory.platform.TargetHorizontalTiltAngleUUID) {
  		var airdir = r.airdirh;
			if (airdir) {
				callback(undefined, horizontalTiltAngle[airdir]);
			} else {
				callback(undefined, undefined);
			}
  		return;
  	} else if (characteristic.UUID == homebridgeAccessory.platform.CurrentVerticalTiltAngleUUID ||
  			   characteristic.UUID == homebridgeAccessory.platform.TargetVerticalTiltAngleUUID) {
			var airdir = r.airdir;
			if (airdir) {
				callback(undefined, verticalTiltAngle[airdir]);
			} else {
				callback(undefined, undefined);
			}
  		return;
  	} else {
  		callback(undefined, 0);
  		return;
  	}
  },
  setAccessoryValue: function(callback, characteristic, service, homebridgeAccessory, value) {
		const unit = this.units.find(function(unit){
			return homebridgeAccessory.id == unit.unitid;
		})

		if (!unit){
			callback()
			return;
		}

    var commands = null;

  	if (characteristic.UUID == homebridgeAccessory.platform.TargetHeatingCoolingStateUUID) {
  		switch (value) {
  			case Characteristic.TargetHeatingCoolingState.OFF:
          // homebridgeAccessory.airInfo.power = "q"
  				commands = "PW0";
  				break;
  			case Characteristic.TargetHeatingCoolingState.HEAT:
          // homebridgeAccessory.airInfo.power = "on"
          // homebridgeAccessory.airInfo.mode = "1"
  				commands = "PW1,MD1"
  				break;
  			case Characteristic.TargetHeatingCoolingState.COOL:
          // homebridgeAccessory.airInfo.power = "on"
          // homebridgeAccessory.airInfo.mode = "3"
  				commands = "PW1,MD3"
  				break;
  			case Characteristic.TargetHeatingCoolingState.AUTO:
          // homebridgeAccessory.airInfo.power = "on"
          // homebridgeAccessory.airInfo.mode = "8"
  				commands = "PW1,MD8"
  				break;
  			default:
  				callback();
  				return;
  		}
  	} else if (characteristic.UUID == homebridgeAccessory.platform.TargetTemperatureUUID) {
      // homebridgeAccessory.state.settemp = `${value}`
  		commands = `TS${value}`
  	} else if (characteristic.UUID == homebridgeAccessory.platform.TemperatureDisplayUnits) {
  		var UseFahrenheit=false;
  		if (value == Characteristic.TemperatureDisplayUnits.FAHRENHEIT)
  			UseFahrenheit = true;
  		homebridgeAccessory.platform.updateApplicationOptions(UseFahrenheit);
  		homebridgeAccessory.platform.UseFahrenheit = UseFahrenheit;
  		callback();
  		return;
  	} else if (characteristic.UUID == homebridgeAccessory.platform.RotationSpeedUUID) {
			var fanstage = homebridgeAccessory.capabilities.fanstage;
			var fanSpeed = (value/100.0 * fanstage).toFixed(0);
			commands = `FS${fanSpeed}`;
  	// } else if (characteristic.UUID == homebridgeAccessory.platform.TargetHorizontalTiltAngleUUID) {
  	// 	r.VaneHorizontal = ((value + 90.0)/45.0 + 1.0).toFixed(0);
  	// 	r.EffectiveFlags = 256;
  	// } else if (characteristic.UUID == homebridgeAccessory.platform.TargetVerticalTiltAngleUUID) {
  	// 	r.VaneVertical = ((value + 90.0) / 45.0 + 1.0).toFixed(0);
  	// 	r.EffectiveFlags = 16;
  	} else {
  		callback();
  		return;
  	}

		this.sendCommand(unit, commands, callback);
    //
    //
    // var url = "https://api.melview.net/api/unitcommand.aspx";
  	// var body = JSON.stringify({
    //   unitid: r.unitid,
    //   v: 2,
    //   commands: commands,
    //   lc: 1,
    //   localip: homebridgeAccessory.platform.localip
    // });
    //
    // request({
    //   url: url,
    //   headers: {"Cookie": homebridgeAccessory.platform.cookie},
    //   method: "post",
    //   form: body
    // }, function(err, response) {
    //   if (err) {
    // 		homebridgeAccessory.platform.log("There was a problem sending commands to: " + url);
    // 		homebridgeAccessory.platform.log(err);
  	//   }
    //   else {
    //     homebridgeAccessory.platform.log("response: " + JSON.stringify(response));
    //   }
    //
  	//   callback();
    // });
  },
  getInformationService: function(homebridgeAccessory) {
    var informationService = new Service.AccessoryInformation();
    informationService
                .setCharacteristic(Characteristic.Name, homebridgeAccessory.name)
				.setCharacteristic(Characteristic.Manufacturer, homebridgeAccessory.manufacturer)
			    .setCharacteristic(Characteristic.Model, homebridgeAccessory.model)
			    .setCharacteristic(Characteristic.SerialNumber, homebridgeAccessory.serialNumber);
  	return informationService;
  },
  bindCharacteristicEvents: function(characteristic, service, homebridgeAccessory) {
		characteristic.on('set', function(value, callback, context) {
			homebridgeAccessory.platform.setAccessoryValue(callback, characteristic, service, homebridgeAccessory, value);
		}.bind(this) );

    characteristic.on('get', function(callback) {
  		homebridgeAccessory.platform.getAccessoryValue(callback, characteristic, service, homebridgeAccessory);
     }.bind(this) );
  },
  getServices: function(homebridgeAccessory) {
  	var services = [];
  	var informationService = homebridgeAccessory.platform.getInformationService(homebridgeAccessory);
  	services.push(informationService);
  	for (var s = 0; s < homebridgeAccessory.services.length; s++) {
		var service = homebridgeAccessory.services[s];
		for (var i=0; i < service.characteristics.length; i++) {
			var characteristic = service.controlService.getCharacteristic(service.characteristics[i]);
			if (characteristic == undefined)
				characteristic = service.controlService.addCharacteristic(service.characteristics[i]);
			homebridgeAccessory.platform.bindCharacteristicEvents(characteristic, service, homebridgeAccessory);
		}
		services.push(service.controlService);
    }
    return services;
  }
}

function MelviewBridgedAccessory(services) {
    this.services = services;
}

MelviewBridgedAccessory.prototype = {
  	getServices: function() {
		var services = [];
		var informationService = this.platform.getInformationService(this);
		services.push(informationService);
		for (var s = 0; s < this.services.length; s++) {
			var service = this.services[s];
			for (var i=0; i < service.characteristics.length; i++) {
				var characteristic = service.controlService.getCharacteristic(service.characteristics[i]);
				if (characteristic == undefined)
					characteristic = service.controlService.addCharacteristic(service.characteristics[i]);
				this.platform.bindCharacteristicEvents(characteristic, service, this);
			}
			services.push(service.controlService);
		}
		return services;
	}
}
